package router

import (
	"net/http"
	

	routes "../../../pkg/types/routes"
	HomeHandler "../../controllers/home"
	NewPostHandler "../../controllers/newpost"
	UserHandler "../../controllers/user"
)

func Middleware(next http.Handler) http.Handler {


	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}

func GetRoutes() routes.Routes {
	return routes.Routes{
		routes.Route{"Home", "GET", "/", HomeHandler.Index},
		routes.Route{"User", "GET", "/user", UserHandler.Index},
		routes.Route{"NewPost", "GET", "/newpost", NewPostHandler.Index},
	}
}
