package router

import (
	"../../../../pkg/types/routes"
	StatusHandler "../status"
	"net/http"
	"log"
)

func Middleware(next http.Handler) http.Handler{
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		token := r.Header.Get("X-App-Token")
		if len(token) < 1{
			http.Error(w, "Not authorized", http.StatusUnauthorized)
			return
		}
		log.Println("Inside V1 Middleware")
		next.ServeHTTP(w,r)
	})
}

func GetRoutes() routes.Routes{
	return routes.Routes{
		routes.Route{"Status", "GET", "/", StatusHandler.Index},
	}	
}