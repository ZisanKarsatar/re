package user

import (
    "fmt"
	"io/ioutil"
    "net/http"
)

func loadFile(fileName string) (string, error) {
    bytes, err := ioutil.ReadFile(fileName)
    if err != nil {
        return "", err
    }
    return string(bytes), nil
}

func Index(w http.ResponseWriter ,r *http.Request) {	
	//w.Write([]byte("New post sayfasıdırrrrr"))

	var body, _ = loadFile("user.html")
    fmt.Fprintf(w, body)
}
