import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import App from './App.vue'
import Home from './components/Home.vue'
import PostProfile from './components/PostProfile.vue'
import NewPost from './components/NewPost.vue'

const router = new VueRouter({
  routes: [
    { path: '/', component: Home, name: 'home' },
    { path: '/post/:id', component: PostProfile, name: 'postProfile' },
    { path: '/newpost', component: NewPost, name: 'postProfile' },
    
  ],
  mode: 'history'
})

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
