## Reina-Posts-System

<h6>Kartaca Staj Ödevi</h6>

Reina Posts System(tree)
|
\-client
\-api

<h2>CLIENT</h2>
<div class="back" style="background-color:grey">
    <ul>
    <li>Projenin client kısmı vue  ile hazırlanmıştır. </li>
    <li>Router ile yönlendirmeler yapılır. </li>
    <li>Bilgiler json sunucu tarafından dinamik olarak kullanılmaktadır.</li>
    <li>Yönlendirmeler vue.router ile sağlanmakta. Bir çok farklı kullanıcı için profil sayfasına erişilmektedir.</li>
    </ul>
    <div>   
    <h3>Çalıştırmak için:</h3>
    <ul>
        <li><b>cd client</b></li>
        <li><b>npm run dev</b></li>
    </ul> 
    </div>
</div>

<a href="https://my-json-server.typicode.com/zisankarsatar/demo">JSON SUNUCU</a>

<h2>API</h2>
<div class="back" style="background-color:grey">
    <ul>
    <li>Projenin api kısmı golang dili ile yazılmıştır. </li>
    <li>Router ile yönlendirmeler yapılır. </li>
    <li>Bilgiler html klasörünün içinde statik olarak tutulmaktadır.</li>
    <li>Tek kullanıcı için profil sayfasına erişilmektedir.</li>
    </ul>
    <div>   
    <h3>Çalıştırmak için:</h3>
    <ul>
        <li><b>cd api</b></li>
        <li><b>go run main.go</b></li>
    </ul> 
    </div>
</div>

<h2>CLIENT KISMI İÇİN GİF</h2>

![home](/uploads/60ce44d9f1a1ad6b6cb66269f4730feb/home.gif)


![newnote](/uploads/45eb579c4767f0177013920a537e18ea/newnote.gif)


<h2>API İÇİN EKRAN GÖRÜNTÜLERİ</h2>

![home](/uploads/8a184c39ac346f2d6e26aeb989ddef45/home.PNG)

![user](/uploads/1068afefbff85e3b583ad861b7682bcc/user.PNG)

![newpost](/uploads/3bf80ee87d50d54fd742bc0893fb3faf/newpost.PNG)


DOCKER ÇALIŞMA




